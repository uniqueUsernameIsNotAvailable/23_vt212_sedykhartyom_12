#ifndef XMLPARSER_H
#define XMLPARSER_H

#include <QtXml>
#include <vector>
#include <QString>
#include "Photo.h"
using namespace std;


class Parser
{
public:
	Parser();
	vector<Photo> getAlbumsFromXml(QFile* xmlFile);
	QString saveListToXml(vector<Photo> list);
};

#endif // XMLPARSER_H
