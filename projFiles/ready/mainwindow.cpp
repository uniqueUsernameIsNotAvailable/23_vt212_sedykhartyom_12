﻿#include <QtGui>
#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget* parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	//init
	albumChanged = false;
	newFile = true;
	curPic = 0;

	//set up the label
	ui->setupUi(this);
	imageLabel = new QLabel;
	imageLabel->setBackgroundRole(QPalette::Base);
	imageLabel->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
	imageLabel->setScaledContents(true);

	//scrolling
	scrollArea = new QScrollArea;
	scrollArea->setBackgroundRole(QPalette::Dark);
	scrollArea->setWidget(imageLabel);

	//center
	widget = new QWidget;
	layout = new QVBoxLayout(widget);
	layout->addWidget(scrollArea);
	setCentralWidget(widget);

	//create actions, menus, toolbars, and the status bar
	initActions();
	initMenus();
	initTB();
	initSB();

	//set title and size
	setWindowTitle(tr("OOP_PROJECT"));
	resize(1280, 720);

	this->ui->mainToolBar->hide();
}

//make an album with checks
void MainWindow::newAlbum()
{
	if (albumChanged)
	{
		int response = QMessageBox::information(this, tr("Save"), tr("Album updated. Save?"),
		                                        QMessageBox::Ok | QMessageBox::Discard | QMessageBox::Cancel,
		                                        QMessageBox::Cancel);

		switch (response)
		{
		case QMessageBox::Ok:
			save();
			break;
		case QMessageBox::Discard:
			break;
		case QMessageBox::Cancel:
			return;
			break;
		}
	}
	//create new albums
	album = vector<Photo>();

	//image correction
	image = QImage();
	newFile = true;
	imageLabel->setPixmap(QPixmap::fromImage(image));
	imageLabel->resize(image.width(), image.height());

	//set enabled as necessary
	//enableImageEdits(false);
	deletePhotoAct->setEnabled(false);
	updateMoveEnables();
	saveAct->setEnabled(false);
	saveAsAct->setEnabled(false);
}

//open an album with checks
void MainWindow::open()
{
	//ask the user
	if (albumChanged)
	{
		int response = QMessageBox::information(this, tr("Save"),
		                                        tr("This album has been changed. Save changes?"),
		                                        QMessageBox::Ok | QMessageBox::Discard | QMessageBox::Cancel,
		                                        QMessageBox::Cancel);

		switch (response)
		{
		case QMessageBox::Ok:
			save();
			break;
		case QMessageBox::Discard:
			break;
		case QMessageBox::Cancel:
			return; //don't open a file just return
			break;
		}
	}

	QString fileName =
		QFileDialog::getOpenFileName(this, tr("Open File"), QDir::currentPath(), tr("XML files (*.xml)"));
	if (!fileName.isEmpty())
	{
		//set the filename
		filename = fileName;
		newFile = false;

		//parse the file
		QFile* file = new QFile(filename);
		Parser parse;
		curPic = 0;
		album = parse.getAlbumsFromXml(file);

		//set the current image and set enables as needed
		if (album.size() != 0)
		{
			image = QImage(album[0].FileName);
			imageLabel->setPixmap(QPixmap::fromImage(image));
			imageLabel->resize(image.width(), image.height());
			deletePhotoAct->setEnabled(true);
		}
		else
		{
			deletePhotoAct->setEnabled(false);
		}
		if (image.isNull())
		{
			QMessageBox::information(this, tr("Image Viewer"), tr("Cannot load %1.").arg(filename));
			return;
		}
		imageLabel->setPixmap(QPixmap::fromImage(image));
		scaleFactor = 1.0;
		//updateActions();
	}
	//update enabled as needed
	updateMoveEnables();
	saveAct->setEnabled(false);
	saveAsAct->setEnabled(false);
}

//save an album with checks
void MainWindow::save()
{
	if (newFile)
	{
		saveAs();
	}
	else
	{
		//intialize the file.
		QFile file;
		newFile = false;
		file.setFileName(filename);
		file.open(QIODevice::ReadWrite);

		//parse the file
		Parser parse;
		QString string = parse.saveListToXml(album);
		QTextStream stream(&file);

		//output the file
		stream << string;
		file.close();
		albumChanged = false;
	}
}

//new album save
void MainWindow::saveAs()
{
	//save the album
	QFile file;
	filename = QFileDialog::getSaveFileName(this, tr("Save File"), QDir::currentPath(), tr("XML files (*.xml)"));
	newFile = false;
	file.setFileName(filename);
	file.open(QIODevice::ReadWrite);

	//parse the album to get the xml format
	Parser parse;
	QString string = parse.saveListToXml(album);

	//output it to file
	QTextStream stream(&file);
	stream << string;
	file.close();
	albumChanged = false;
}

//close an album with checks
void MainWindow::close()
{
	if (albumChanged) //ask user to save changes
	{
		int response = QMessageBox::information(this, tr("Save"),
		                                        tr("Save changes before closing?"),
		                                        QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel,
		                                        QMessageBox::Cancel);

		switch (response)
		{
		case QMessageBox::Save:
			{
				save();
				QApplication::quit();
				break;
			}
		case QMessageBox::Discard:
			{
				QApplication::quit();
				break;
			}
		case QMessageBox::Cancel:
			break;
		default:
			break;
		}
	}
	else
	{
		//quit program
		QApplication::quit();
	}
}

//push back photo
void MainWindow::addPhoto()
{
	Photo photo;
	QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), QDir::currentPath(),
	                                                tr("Image Files (*.png *.jpg *.bmp)"));
	if (fileName != "")
	{
		//set the image
		image = QImage(fileName);
		imageLabel->setPixmap(QPixmap::fromImage(image));
		imageLabel->resize(image.width(), image.height());

		//since a photo was added image edits is possible
		photo.FileName = fileName;

		//add photo to end of list and update current picture
		album.push_back(photo);
		curPic = album.size() - 1;
		albumChanged = true;
	}
	if (album.size() != 0)
	{
		deletePhotoAct->setEnabled(true);
	}
	//set enables as needed
	updateMoveEnables();
	saveAct->setEnabled(true);
	saveAsAct->setEnabled(true);
}

//delete photo
void MainWindow::deletePhoto()
{
	if (album.size() != 0)
	{
		//prompt the user to delete the phtot
		int response = QMessageBox::information(this, tr("Delete"),
		                                        tr("Delete current photo from album?"),
		                                        QMessageBox::Ok | QMessageBox::Cancel, QMessageBox::Cancel);

		switch (response)
		{
		case QMessageBox::Ok:
			//delete photo from list
			for (unsigned int i = curPic; i + 1 < album.size(); i++)
			{
				album[i + 1] = album[i];
			}
			album.pop_back();

		//open next photo for viewing
			if (curPic == album.size())
			{
				curPic = album.size() - 1;
			}
			if (album.size() != 0)
			{
				//set the next image in the list
				image = QImage(album[curPic].FileName);
				imageLabel->setPixmap(QPixmap::fromImage(image));
				imageLabel->resize(image.width(), image.height());
			}
			else
			{
				image = QImage();
				imageLabel->setPixmap(QPixmap::fromImage(image));
				imageLabel->resize(image.width(), image.height());
			}
			albumChanged = true;
			break;
		case QMessageBox::Cancel:
			break;
		}
	}
	if (album.size() == 0)
	{
		//check enables
		deletePhotoAct->setEnabled(false);
	}
	//enable as necessary
	updateMoveEnables();
	saveAct->setEnabled(true);
	saveAsAct->setEnabled(true);
}


//move to next
void MainWindow::nextPhoto()
{
	//can only move to the next photo if not at the end
	if (curPic != album.size() - 1 && album.size() != 0)
	{
		//move to the next picture
		curPic = curPic + 1;
		image = QImage(album[curPic].FileName);
		imageLabel->setPixmap(QPixmap::fromImage(image));
		imageLabel->resize(image.width(), image.height());
		//pictureChanged = false;
	}
	//update enables
	updateMoveEnables();
}

//move to prev
void MainWindow::previousPhoto()
{
	if (curPic != 0)
	{
		//move back an image.
		curPic = curPic - 1;
		image = QImage(album[curPic].FileName);
		imageLabel->setPixmap(QPixmap::fromImage(image));
		imageLabel->resize(image.width(), image.height());
		//pictureChanged = false;
	}
	//update enables as necessary
	updateMoveEnables();
}

//move forward in album
void MainWindow::moveForward()
{
	//make sure we don't step off the end of the vector
	if (curPic + 1 < album.size())
	{
		//swap the photos
		Photo temp = album[curPic];
		album[curPic] = album[curPic + 1];
		album[curPic + 1] = temp;
		albumChanged = true;
		curPic = curPic + 1;
	}
	//update enables
	updateMoveEnables();
	saveAct->setEnabled(true);
	saveAsAct->setEnabled(true);
}

//move backward in album
void MainWindow::moveBackward()
{
	//check that we don't step off the end of the vector
	if (curPic != 0 && album.size() != 0)
	{
		Photo temp = album[curPic];
		album[curPic] = album[curPic - 1];
		album[curPic - 1] = temp;
		albumChanged = true;
		curPic = curPic - 1;
	}
	//update enables
	updateMoveEnables();
	saveAct->setEnabled(true);
	saveAsAct->setEnabled(true);
}

// рассказываю о себе)))
void MainWindow::about()
{
	QString str;
	str = "Project Gallery made for OOP discipline. Made with love <3."
		"\n\nAlbums, photos, descriptions.";
	QMessageBox::about(this, tr("About Project"), str);
}


//actions with signals creator
void MainWindow::initActions()
{
	newAct = new QAction(QIcon(":/icon/images/new.png"), tr("&New..."), this);
	newAct->setStatusTip(tr("Create a new album"));
	connect(newAct, SIGNAL(triggered()), this, SLOT(newAlbum()));

	openAct = new QAction(QIcon(":/icon/images/open.png"), tr("&Open..."), this);
	openAct->setStatusTip(tr("Open a file"));
	connect(openAct, SIGNAL(triggered()), this, SLOT(open()));

	saveAct = new QAction(QIcon(":/icon/images/save.png"), tr("&Save..."), this);
	saveAct->setStatusTip(tr("Save album"));
	saveAct->setEnabled(false);
	connect(saveAct, SIGNAL(triggered()), this, SLOT(save()));

	saveAsAct = new QAction("Save...", this);
	saveAsAct->setStatusTip(tr("Save album as"));
	saveAsAct->setEnabled(false);
	connect(saveAsAct, SIGNAL(triggered()), this, SLOT(saveAs()));

	exitAct = new QAction(QIcon(":/icon/images/exit.png"), tr("E&xit"), this);
	exitAct->setStatusTip(tr("Exit the program"));
	connect(exitAct, SIGNAL(triggered()), this, SLOT(close()));

	addPhotoAct = new QAction(QIcon(":/icon/images/add-new.png"), tr("&Add Photo"), this);
	addPhotoAct->setStatusTip(tr("Add photo to album"));
	connect(addPhotoAct, SIGNAL(triggered()), this, SLOT(addPhoto()));

	deletePhotoAct = new QAction(QIcon(":/icon/images/delete.png"), tr("&Delete Photo"), this);
	deletePhotoAct->setStatusTip(tr("Delete photo from album"));
	deletePhotoAct->setEnabled(false);
	connect(deletePhotoAct, SIGNAL(triggered()), this, SLOT(deletePhoto()));

	nextPhotoAct = new QAction(QIcon(":/icon/images/arrow-right.png"), tr("&Next Photo"), this);
	nextPhotoAct->setStatusTip(tr("View next photo"));
	nextPhotoAct->setEnabled(false);
	connect(nextPhotoAct, SIGNAL(triggered()), this, SLOT(nextPhoto()));

	prevPhotoAct = new QAction(QIcon(":/icon/images/arrow-left.png"), tr("&Previous Photo"), this);
	prevPhotoAct->setStatusTip(tr("View previous photo"));
	prevPhotoAct->setEnabled(false);
	connect(prevPhotoAct, SIGNAL(triggered()), this, SLOT(previousPhoto()));

	moveForwardAct = new QAction(QIcon(":/icon/images/arrow-down.png"), tr("&Move Photo Forward"), this);
	moveForwardAct->setStatusTip(tr("Move photo forward in album"));
	moveForwardAct->setEnabled(false);
	connect(moveForwardAct, SIGNAL(triggered()), this, SLOT(moveForward()));

	moveBackwardAct = new QAction(QIcon(":/icon/images/arrow-up.png"), tr("&Move Photo Backward"), this);
	moveBackwardAct->setStatusTip(tr("Move photo backward in album"));
	moveBackwardAct->setEnabled(false);
	connect(moveBackwardAct, SIGNAL(triggered()), this, SLOT(moveBackward()));

	aboutAct = new QAction(tr("&About"), this);
	aboutAct->setStatusTip(tr("About this program"));
	connect(aboutAct, SIGNAL(triggered()), this, SLOT(about()));
}

//menu creator
void MainWindow::initMenus()
{
	fileMenu = new QMenu(tr("&File"), this);
	fileMenu->addAction(newAct);
	fileMenu->addAction(openAct);
	fileMenu->addAction(saveAct);
	fileMenu->addAction(saveAsAct);
	fileMenu->addSeparator();
	fileMenu->addAction(exitAct);

	editMenu = new QMenu(tr("&Edit"), this);
	editMenu->addAction(addPhotoAct);
	editMenu->addAction(deletePhotoAct);
	editMenu->addSeparator();

	editMenu->addSeparator();
	editMenu->addAction(nextPhotoAct);
	editMenu->addAction(prevPhotoAct);
	editMenu->addAction(moveForwardAct);
	editMenu->addAction(moveBackwardAct);

	helpMenu = new QMenu(tr("&Help"), this);
	helpMenu->addAction(aboutAct);

	menuBar()->addMenu(fileMenu);
	menuBar()->addMenu(editMenu);
	menuBar()->addMenu(helpMenu);
}

//toolbars creator
void MainWindow::initTB()
{
	fileToolBar = addToolBar(tr("File"));
	fileToolBar->addAction(newAct);
	fileToolBar->addAction(openAct);
	fileToolBar->addAction(saveAct);
	fileToolBar->addSeparator();
	fileToolBar->addAction(exitAct);

	editToolBar = addToolBar(tr("Edit"));
	editToolBar->addAction(addPhotoAct);
	editToolBar->addAction(deletePhotoAct);
	editToolBar->addSeparator();
	editToolBar->addAction(prevPhotoAct);
	editToolBar->addAction(nextPhotoAct);
	editToolBar->addAction(moveBackwardAct);
	editToolBar->addAction(moveForwardAct);
}

//statusBar
void MainWindow::initSB()
{
	statusBar()->showMessage(tr("Ready"));
}


//scrollbar logic
void MainWindow::updScrBar(QScrollBar* scrollBar, double factor)
{
	scrollBar->setValue(int(factor * scrollBar->value() + ((factor - 1) * scrollBar->pageStep() / 2)));
}


//moving logic
void MainWindow::updateMoveEnables()
{
	if (album.size() == 0) //if there are no photos disable them all
	{
		moveForwardAct->setEnabled(false);
		nextPhotoAct->setEnabled(false);
		prevPhotoAct->setEnabled(false);
		moveBackwardAct->setEnabled(false);
		return;
	}

	if (curPic + 1 == album.size()) //should only move backward
	{
		moveForwardAct->setEnabled(false);
		nextPhotoAct->setEnabled(false);
		if (album.size() == 1)
		{
			prevPhotoAct->setEnabled(false);
			moveBackwardAct->setEnabled(false);
		}
		else
		{
			prevPhotoAct->setEnabled(true);
			moveBackwardAct->setEnabled(true);
		}
		return;
	}

	if (curPic == 0) //should only move forward
	{
		moveBackwardAct->setEnabled(false);
		prevPhotoAct->setEnabled(false);
		if (album.size() == 1)
		{
			nextPhotoAct->setEnabled(false);
			moveForwardAct->setEnabled(false);
		}
		else
		{
			nextPhotoAct->setEnabled(true);
			moveForwardAct->setEnabled(true);
		}
		return;
	}

	//if neither of these are the case, then enable them all.
	moveBackwardAct->setEnabled(true);
	prevPhotoAct->setEnabled(true);
	nextPhotoAct->setEnabled(true);
	moveForwardAct->setEnabled(true);
}


//destructor
MainWindow::~MainWindow()
{
	delete ui;
}
