#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "pictureedits.h"
#include "resizewindow.h"
#include "xmlparser.h"
#include "Photo.h"
#include <QLayout>

QT_BEGIN_NAMESPACE
class QAction;
class QLabel;
class QMenu;
class QScrollArea;
class QScrollBar;
class QPlainTextEdit;
class QPixmap;
class QRubberBand;
class QMatrix;
QT_END_NAMESPACE

namespace Ui
{
	class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget* parent = 0);
	~MainWindow();

private slots:
	void newAlbum();
	void open();
	void save();
	void saveAs();
	void close();

	void addPhoto();
	void deletePhoto();
	void nextPhoto();
	void previousPhoto();
	void moveForward();
	void moveBackward();

	void about();

private:
	Ui::MainWindow* ui;

	void initActions();
	void initMenus();
	void initTB();
	void initSB();
	void updateMoveEnables();
	void updScrBar(QScrollBar* scrollBar, double factor);

	//widgets that make up the main window
	QLabel* imageLabel;
	QImage image;
	QScrollArea* scrollArea;
	QVBoxLayout* layout;
	QWidget* widget;
	double scaleFactor;

	//actions
	QAction* newAct;
	QAction* openAct;
	QAction* saveAct;
	QAction* saveAsAct;
	QAction* exitAct;

	QAction* addPhotoAct;
	QAction* deletePhotoAct;
	QAction* nextPhotoAct;
	QAction* prevPhotoAct;
	QAction* moveForwardAct;
	QAction* moveBackwardAct;

	QAction* aboutAct;

	//menus
	QMenu* fileMenu;
	QMenu* editMenu;
	QMenu* helpMenu;

	//toolbars
	QToolBar* fileToolBar;
	QToolBar* editToolBar;

	//misc
	bool albumChanged;
	bool newFile;
	unsigned int curPic;
	QString filename;
	vector<Photo> album;
};

#endif // MAINWINDOW_H
