#include "xmlparser.h"
#include <QString>

//constructor
Parser::Parser() //constructor
{
}


//xml->parsing->album
vector<Photo> Parser::getAlbumsFromXml(QFile* xmlFile)
{
	//file to parse
	xmlFile->open(QIODevice::ReadOnly);
	QDomDocument doc("mydocument");
	doc.setContent(xmlFile);
	xmlFile->close();

	//list of photos
	vector<Photo> album = vector<Photo>();

	//parsing
	QDomElement docElem = doc.documentElement(); //album
	for (QDomNode n = docElem.firstChild(); !n.isNull(); n = n.nextSibling())
	{
		QDomElement e = n.toElement(); //photo
		Photo photo;
		for (QDomNode m = e.firstChild(); !m.isNull(); m = m.nextSibling())
		{
			if (!e.isNull())
			{
				QDomElement f = m.toElement(); //elements of photo
				if (!f.isNull())
				{
					QString tag = f.tagName();
					if (tag == "filename")
					{
						photo.FileName = f.text();
					}
				}
			}
		}
		album.push_back(photo);
	}

	return album;
}


QString Parser::saveListToXml(vector<Photo> photos)
{
	//create a string for output
	QString outString;
	QDate date;
	outString += "<album>\n";
	//add each item to the album
	for (unsigned int i = 0; i < photos.size(); i++)
	{
		Photo a = photos[i];
		if (a.FileName != "") //photo must have a name
		{
			outString += "<photo>\n";
			outString += "<filename>" + a.FileName + "</filename>\n";
			outString += "</photo>\n";
		}
	}
	outString += "</album>\n";
	return outString;
}
