#include "descriptionwindow.h"
#include "ui_descriptionwindow.h"


DescriptionWindow::DescriptionWindow():
	ui(new Ui::DescriptionWindow)
{
	ui->setupUi(this);


	setWindowTitle(tr("Edit Image Description"));
}


void DescriptionWindow::show()
{
	QDialog::show();
	ui->locationEdit->setText(*location);
	ui->descriptionEdit->setText(*description);
}

//storage for info
void DescriptionWindow::ok()
{
	*location = ui->locationEdit->text();
	*description = ui->descriptionEdit->text();
	emit descChanged();
}


DescriptionWindow::~DescriptionWindow()
{
	delete ui;
}
